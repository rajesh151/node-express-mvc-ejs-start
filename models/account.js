/** 
*  Product model
*  Describes the characteristics of each attribute in a product resource.
*@author BHUVANESH NAKKA
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  accountKey: { type: String, required: true, unique: true, default: 'account' },
  description: { type: String, required: false, default: 'description' },
  balance: { type: Number, required: true, default: 9.99, min: 0, max: 100000 }
})

module.exports = mongoose.model('Account', AccountSchema)
